<%-- 
    Document   : edit
    Created on : 13-dic-2016, 21:51:50
    Author     : carlo
--%>


<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Edici�n de cliente</h1>

    <jsp:useBean id="clientes" class="model.Client" scope="request"/>  

    <form action="/mvcJava/client/update" method="post">
        <label >Id</label>
        <input type="text" name="id" readonly="readonly" 
               value="<%= clientes.getId()%>"><br>
        <label >Nombre</label>
        <input type="text" name="name"  
               value="<%= clientes.getName()%>"><br>
        <label >Direccion</label>
        <input type="text" name="adddress" 
               value="<%= clientes.getAdddress()%>"><br>
        <label >Telefono</label>
        <input type="number" name="phone"  
               value="<%= clientes.getPhone()%>"><br>
        <label >Creditos</label>
        <input type="number" name="credit"  
               value="<%= clientes.getCredit()%>"><br>
        <input type="submit" value="Guardar">
    </form>

</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>