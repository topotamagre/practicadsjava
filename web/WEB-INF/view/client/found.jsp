<%-- 
    Document   : found
    Created on : 13-dic-2016, 21:41:10
    Author     : carlo
--%>

<%@page import="model.Client"%>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<jsp:useBean id="client" class="Client" scope="request"/>  

<div id="content">        
    <h1>Localizar Cliente</h1>
    <ul>
        <li><%= client.getId() %></li>
        <li><%= client.getName() %></li>
        <li><%= client.getAdddress() %></li>
        <li><%= client.getPhone() %></li>
        <li><%= client.getCredit() %></li>
    </ul>
</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>