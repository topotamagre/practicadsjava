<%-- 
    Document   : insert
    Created on : 13-dic-2016, 21:49:18
    Author     : carlo
--%>


<%@page import="model.Client"%>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Alta de Cliente</h1>

    <jsp:useBean id="clientes" class="model.Client" scope="request"/>  
    <%--<jsp:useBean id="count" class="Integer" scope="request"/>--%>  

    <% if (clientes == null) { %>
    <p>Registro no guardado</p> 
    <% } else {%>
    <ul>
        <li><%= clientes.getName()%></li>
        <li><%= clientes.getAdddress()%></li>
        <li><%= clientes.getPhone()%></li>
        <li><%= clientes.getCredit()%></li>
    </ul>
    <% }%>    


</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>