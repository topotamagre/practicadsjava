<%-- 
    Document   : index
    Created on : 13-dic-2016, 19:13:51
    Author     : usuario
--%>

<%@page import="model.Client"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Lista de Clientes</h1>
    
<!--    
    <% 
        if (null != request.getAttribute("countDelete")) {
            Integer count = (Integer) request.getAttribute("countDelete");
            out.println("Borrado: " + count + " registro(s)" );
        };
    
    %>
        -->
        
    
    <jsp:useBean id="clientes" class="ArrayList<model.Client>" scope="request"/>  
    
    <p><a href="<%= request.getContextPath()%>/client/search">Buscar Cliente</a></p>

    <table>
        <tr>
            <th>Id</th>
            <th>Clientes</th>
            <th>Acción</th>
        </tr>
    <%
        Iterator<model.Client> iterator = clientes.iterator();
        while (iterator.hasNext()) {
            Client clientes = iterator.next();%>
    <tr>
        <td><%= clientes.getName() %></td>
        <td><%= clientes.getAdddress() %></td>
        <td><%= clientes.getPhone() %></td>
        <td><%= clientes.getCredit() %></td>
        <td><a href="<%= request.getContextPath()%>/client/edit/<%= clientes.getId() %>">Editar</a> 
        </td>
    </tr>
    <%
        }
    %>
    </table>
    
    <a href="<%= request.getContextPath()%>/client/index/1"> 1 </a> -
    <a href="<%= request.getContextPath()%>/client/index/2"> 2 </a> -
    <a href="<%= request.getContextPath()%>/client/index/3"> 3 </a> 

</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>
