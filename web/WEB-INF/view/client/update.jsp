<%-- 
    Document   : update
    Created on : 13-dic-2016, 21:55:53
    Author     : carlo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pagina de Edicion</title>
    </head>
    <body>
        <h1>Cliente guardado</h1>
        <jsp:useBean id="clientes" class="model.Client" scope="request"/>  
        <jsp:useBean id="count" class="Integer" scope="request"/>  
        
        <% if (count.equals(0)) { %>
        <p>Registro no actualizado</p> 
        <% } else { %>
        <ul>
            <li><%= clientes.getName()%></li>
            <li><%= clientes.getAdddress()%></li>
            <li><%= clientes.getPhone()%></li>
            <li><%= clientes.getCredit()%></li>
        </ul>
        <% } %>
    </body>
</html>
