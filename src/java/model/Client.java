/*
 * Java Bean Client
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Carlos
 */
public class Client implements Serializable
{
    private int id;
    private String name;
    private String adddress;
    private int phone;
    private double credit;



    @Override
    public String toString() {
        return "Client{" + "name=" + name + ", address=" + adddress + ", phone=" + phone + ", credit=" + credit +'}';
    }

    public Client(int id, String name, String adddress, int phone, double credit) {
        this.id = id;
        this.name = name;
        this.adddress = adddress;
        this.phone = phone;
        this.credit = credit;
    }
    
    public Client(){
        this.id = 0;
        this.name = "";
        this.adddress = "";
        this.phone = 0;
        this.credit = 0;
                
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdddress() {
        return adddress;
    }

    public void setAdddress(String adddress) {
        this.adddress = adddress;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }



    
}
