package persistence;

/*
 * Clase que se ocupa de la persistencia de USER
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.Client;

/**
 *
 * @author Rafa
 */
public class ClientDAO {
    private static final int PAGE_SIZE = 5;
    private final Object lockOfTheConexion = new Object();
    private Connection connection = null;
    private PreparedStatement stmt = null;
    private static final String CLASS_DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://http://192.168.1.43/mvc";
    private static final Logger LOG = Logger.getLogger(ClientDAO.class.getName());
    private ResultSet rs;
    

    //metodo connect
    public void connect() {
        try {
            Class.forName(ClientDAO.CLASS_DRIVER);
            connection = DriverManager.getConnection(ClientDAO.URL, "usuario", "usuario");
//            stmt = connection.createStatement();            
            if (connection != null) {
                LOG.info("Conexión establecida!");
            } else {
                LOG.severe("Fallo de conexión!");
            }
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo cargar el driver de la base de datos", ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo obtener la conexión a la base de datos", ex);
        }
    }
//metodo disconnect

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }


    //insert
    /**
     *
     * @param client
     * @return int
     * @throws SQLException
     */
    
    public int insert(Client client) {
        try {
            stmt = connection.prepareStatement("insert into client values(DEFAULT,? ,? ,? ,?)");
            stmt.setString(1, client.getName());
            stmt.setString(2, client.getAdddress());
            stmt.setInt(3, client.getPhone());
            stmt.setDouble(4, client.getCredit());

            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public int update(Client client) {
        try {
            stmt = connection.prepareStatement("update user set name=?, adddress=?, phone=?, credit=? where id=?");
            stmt.setString(1, client.getName());
            stmt.setString(2, client.getAdddress());
            stmt.setInt(3, client.getPhone());
            stmt.setDouble(4, client.getCredit());
            stmt.setInt(5, client.getId());

            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public ArrayList<Client> getAll(int page) {
        ArrayList<Client> cliente = null;        
        try {
            stmt = connection.prepareStatement("select * from client limit " + PAGE_SIZE + " offset " + (page - 1) * PAGE_SIZE);
            rs = stmt.executeQuery();
            cliente = new ArrayList();
            int i = 0;
            while (rs.next()) {
                i++;
                Client client = new Client();
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setAdddress(rs.getString("address"));
                client.setPhone(rs.getInt("phone"));
                client.setCredit(rs.getDouble("credit"));
                //other properties
                cliente.add(client);
                LOG.info("Registro fila: " + i);

            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return cliente;
    }
    
    public Client get(int id)
    {
        Client client = new Client();
        LOG.info(client.toString());
        try {
            stmt = connection.prepareStatement("select * from user where id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();            
            if (rs.next()) {
                client.setId(id);
                client.setName(rs.getString("name"));
                client.setAdddress(rs.getString("address"));
                client.setPhone(rs.getInt("phone"));
                client.setCredit(rs.getDouble("credit"));
            } 
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return null;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Falla por otra excepcion", ex);
            return null;
        }
        
        return client;
    }
    //delete
    public int delete(int id) {

        int count = 0;
        try {
            stmt = connection.prepareStatement("delete from client where id=?");
            stmt.setInt(1, id);
            count = stmt.executeUpdate();            
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            count = 0;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Falla por otra excepcion", ex);
            count = 0;
        }
        return count;
    }

}
